import React from 'react';
import "../src/main.css"
import Home from "./components/home";
import Product from "./components/product";
import Login from "./components/login";
import {BrowserRouter, Link, Route} from "react-router-dom";
import ProductAdd from "./components/productAdd";
import Cart from "./components/cart";


function App() {
  return (
      <div className="App">
        <BrowserRouter>
            <div>
                <Route path="/" exact component={Home}></Route>
                <Route path="/product/:id" exact component={Product}></Route>
                <Route path="/login" exact component={Login}></Route>
                <Route path="/productAdd" exact component={ProductAdd}></Route>
                <Route path="/cart" exact component={Cart}></Route>

            </div>
        </BrowserRouter>
      </div>
  );
}

export default App;
