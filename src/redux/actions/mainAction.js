import {resq} from "../../api/request";
import {
    Fetch_User,
    Create_Product,
    Get_Categories,
    ADD_CART_ITEM,
    // Get_ProductDetail
} from "../../helper/helper";
import {flatFields} from "../../helper/tidyDBData";


const actFetchUser = formValue => async dispatch => {    //actFetchUser是一个object
    console.log(formValue);
    try{
        let res = await resq.post('/api/v1/user/auth/login', formValue);
        dispatch({
            type: Fetch_User,
            payload: res.data,
        });
        localStorage.setItem('token',res.data.data.token);   //login以后拿到token并存入localStorage里面
        window.location.href='/productAdd';
    } catch(e) {
        console.log(e);
        return e
    }
};

const actCreateProduct = formValue => async dispatch => {
    console.log('formValue', formValue);
    const token  = localStorage.getItem('token');
    // const headers = {'Authorization': `Bear ${token}`};
    try{
        let res = await resq.post('/api/v1/product/create', formValue, {headers :{'Authorization':`Bear ${token}`}});
        dispatch({
            type: Create_Product,
            payload: res.data,
        });
        window.alert('Add successfully')
        console.log(res.data)
    }catch (e) {
       console.log('Add product error -->', e)
    }
};

const actGetCategories = () => dispatch => {
    const token  = localStorage.getItem('token');
    const headers = {'Authorization': `Bear ${token}`};
    resq.get('/api/v1/product/category', {headers:headers})
        .then(res => {
            dispatch({
            type: Get_Categories,
            payload: res.data
            })
            // console.log("payload ------>", res.data)
        })
        .catch(e => {
        console.log('Category get failed', e)
    })
};


const actAddToCart  = (unitProduct, quantity) => async dispatch => {
    console.log('unitProduct and quantity----->', unitProduct, quantity);
    console.log('type of quantity =====>', typeof quantity);
    // const unitProduct = flatFields(unitProduct);
    dispatch({type: ADD_CART_ITEM, payload: {...unitProduct, quantity}});
    console.log('unitProduct and quantity 2----->', {...unitProduct, quantity})

};

// const actProductDetail = (id) => async dispatch=> {
//     try{
//         let res = await resq.get(`api/v1/product/${id}`);
//         console.log("productDetail ----->", res.data.data.fields);
//         dispatch({
//             type: Get_ProductDetail,
//             payload: res.data
//         })
//     } catch (e) {
//         console.log("Get ProductDetail failed")
//     }
//
// };

export {
    actFetchUser,
    actCreateProduct,
    actGetCategories,
    actAddToCart,
    // actProductDetail,
}

