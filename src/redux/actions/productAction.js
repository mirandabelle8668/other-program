import {
    FETCH_ONE_PRODUCT,
    LOADING,
    LOADING_END,
    Get_ProductList,
    Get_HomeProduct
} from "../../helper/helper";
import {hideMessageBar, showMessageBar} from "../../helper/helper";
import {flatFields} from "../../helper/tidyDBData";
// import {mkHistory} from "../history";
// import {apiFetchAllProduct} from "../api/product";
import {resq} from "../../api/request";


export const  actProductList = () => async dispatch => {
    try{
        let res = await resq.get('/api/v1/product');
        // console.log(res.data);
        dispatch({
            type: Get_ProductList,
            payload: res.data.data,   //res.data返回的是object也即{}的数据，然而productList是[]的array，所以这里是res.data.data

        });
        // window.alert('Get Successfully')
    } catch(e) {
        console.log('Get ProductList error')
    }
};

export const actHomeProduct = () => async dispatch => {
    try{
        let res = await resq.get('/api/v1/product');

        console.log("homeProduct ----->", res.data);
        dispatch({
            type: Get_HomeProduct,
            payload: res.data.data,
        });
        // window.location.href='/';     //为什么加上这个会无限跳转
        console.log('get home product succeed ==>', res.data.data)
    } catch (e) {
        console.log("Create HomeProduct error")
    }
};


const tidyProduct = x => {
    let temp = flatFields(x.fields);
    temp.medias = temp.media.split('|');
    let categories = flatFields(x.categories);
    let profileitem = flatFields(x.profileitems);
    let categoriesObj = [...categories];
    categories = categories.map(x => x.id)[0];
    // parse item
    let keys = [];
    profileitem.forEach(x => {
        let a = -1 === keys.indexOf(x.profile) ? keys.push(x.profile) : 0
    });

    let pi = [];
    keys.forEach(k => {
        let temp = profileitem.filter(x => x.profile === k);
        // let ta = temp.map(x => {profile: x, 'item': temp})
        pi.push({ profile: k, item: temp })
    });

    return { ...temp, categories, profileitem: pi, categoriesObj }
};

export const actFetchOneProduct = (id) => dispatch => {
    // console.log('action fetch all product');
    dispatch({type: LOADING});

    resq.get(`api/v1/product/${id}`).then(res => {

        let data = res.data;
        // console.log(data);
        if (!data.hasOwnProperty('code') ||
            data.code !== 0 ||
            !Array.isArray(data.data)) {
            //todo 1, add top message bar
            let message = data.message ? data.message : `Error fetch all product ${data.code}`;
            dispatch({type: LOADING_END, payload: {needClick: true, message}})
        }

        // console.log(res);
        let productDetail = (data.data);
        // here to tidy and normalize product array
        productDetail = tidyProduct(productDetail);
        // products = products.map(p => {
        //     let categories = p.categories.map(c => flatFields(c))
        //     let profileitems = p.profileitems.map(p => flatFields(p))
        //     p = flatFields(p.fields)
        //     p.media = p.media.split('|')
        //     return {...p, categories, profileitems}
        // })
        console.log('normalized data-->', productDetail);
        dispatch({
            type: FETCH_ONE_PRODUCT,
            payload: productDetail
        });
        dispatch({type: LOADING_END, payload: {needClick: false}});
        console.log('fetch product succeed')
    }).catch(err => {
        console.log('fetch product error', err.message);
        dispatch({type: LOADING_END, payload: {needClick: true, message: `Error: ${err.message}`}})
    })
};

export const addToCart = (id) => {



}



