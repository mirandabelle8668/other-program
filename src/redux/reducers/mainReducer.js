// 如果要用redux-form不管什么项目都要在reducer里面 1.import{reducer as formReducer} from "redux-form"; 2.在combineReducer里面{form:formReducer}
import {
    ADD_CART_ITEM,
    Create_Product, FETCH_ONE_PRODUCT,
    Fetch_User,
    Get_Categories, Get_HomeProduct, Get_ProductList,
    // Get_ProductDetail,

} from "../../helper/helper";
import {combineReducers} from 'redux';
import {reducer as formReducer } from "redux-form";
import productReducer from "./productReducer";

const initialState = {
    streams: [],
    products: [],
    categories: [],
    cartItem: [],
    // productDetail: {},

};

const fetchReducer = (state = initialState, action) => {   //state是我们从python里面拿到的内容，然后对其做相应的action处理
    if (action.type === Fetch_User) {
        return {...state, streams:[...state.streams, action.payload]}   //streams[] 前面部分用...state.streams是为了内存分家，action.payload是后面要添加的
    } else {
        return state
    }
};

const createReducer = (state = initialState, action) => {
    if (action.type === Create_Product) {
        return {...state, products: [...state.products, action.payload]}
    } else {
        return state
    }
};

const categoryReducer = (state = initialState, action) => {
    if (action.type === Get_Categories) {
        return {...state, categories: [...action.payload]}
    } else {
        return state
    }
};

const cartReducer = (state = initialState, action) => {
    console.log('cart item !!!!!!!!!!!', state.cartItem);
    if (action.type === ADD_CART_ITEM) {
        if (state.cartItem.find(item => item.id === action.payload.id)){
            let tempCart = state.cartItem.map(item => item.id === action.payload.id ? {...item, quantity: parseInt(item.quantity) + parseInt(action.payload.quantity)} : {...item});
            return {...state, cartItem: tempCart}
        } else {
            return {...state, cartItem: [action.payload]}
        }
    }
    else{
        return state
    }
};



// const productDetailReducer = (state=initialState, action) => {
//     if (action.type === Get_ProductDetail) {
//         return {...state, productDetail:action.payload}
//     } else {
//         return state
//     }
// };


export default combineReducers({
    fetchReducer,
    createReducer,
    categoryReducer,
    cartReducer,
    // productDetailReducer,
    productReducer,
    form: formReducer
})




