import {FETCH_ONE_PRODUCT, Get_HomeProduct, Get_ProductList} from "../../helper/helper";

const initialState = {
    productDetail: {},
    productList: [],
    homeProduct: [],
};


const productReducer =  (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ONE_PRODUCT:
            return {...state, productDetail: {...action.payload}};
        case Get_ProductList:
            return {...state, productList: [...action.payload]};
        case Get_HomeProduct:
            return {...state, homeProduct: [...action.payload]};
        default:
            return state
    }
};


export default productReducer