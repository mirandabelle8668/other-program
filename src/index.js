import React from 'react';
import ReactDOM from 'react-dom';
import "../src/main.css"
import App from './App';
import {Provider} from "react-redux";  //全局数据管理
import {createStore, applyMiddleware} from "redux";
import reducers from "./redux/reducers/mainReducer"
import thunk from "redux-thunk"
// import ProductAdd from "./components/productAdd";
//store主要使用两个方法getState和subscribe


ReactDOM.render(
    <Provider store={createStore(reducers, applyMiddleware(thunk))}>
        <App />
    </Provider>,
    // <React.StrictMode>
    //   <App />
    // </React.StrictMode>,
document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

