export const Fetch_User= 'Fetch_User';
export const Create_Product= 'Create_Product';
export const Get_Categories = 'Get_Categories';
export const Get_ProductList = 'Get_ProductList';
export const Get_HomeProduct = 'Get_HomeProduct';
// export const Get_ProductDetail = 'Get_ProductDetail';

// Loading progress
export const LOADING = 'LOADING';
export const LOADING_END = 'LOADING_END';

// Product
export const FETCH_ONE_PRODUCT = 'FETCH_ALL_PRODUCT';
// export const CREATE_PRODUCT = 'CREATE_PRODUCT';

// Cart
export const ADD_CART_ITEM = 'ADD_CART_ITEM';

// MessageBar
// export const SHOW_MESSAGE = 'SHOW_MESSAGE';
// // // // export const HIDE_MESSAGE = 'HIDE_MESSAGE';