import React,{Component} from "react";
import {Field, reduxForm} from "redux-form"
import "../main.css"
import {actFetchUser} from "../redux/actions/mainAction";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

class Login extends Component{
    state = {
        username:'',
        password:'',
    };


    onSubmit2 = formValue => {       //onSubmit不能用回调函数   // formValue.preventDefault();
        this.props.actFetchUser(formValue)
    };



    renderInput(field){
        return(
            <div>
                <div>
                    <label className="loginLabel">{field.input.name}</label>
                    {field.meta.error && field.meta.touched &&
                    <span style={{color:'red', fontSize:'12px', marginLeft:'10px'}}>field.meta.error</span>}
                </div>
                <input {...field.input} type={field.type} className="field">
                </input>
            </div>
        )
    }


    render() {
        const {handleSubmit} = this.props;
        return (
            <div>
                <div className="banner-sales">
                    <div className="banner-text">
                        Enjoy Free Shipping Sitewide + 0% Financing Available
                    </div>
                </div>

                <header>
                    <ul className="left-site-list">
                        <li className="active">
                            <Link to={'/'}>Store</Link>
                        </li>
                        <li>
                            <a href="foo">Contract</a>
                        </li>
                    </ul>
                    <div className="center-site-list">
                        <span>
                             <a href="foo">Customer Service</a>
                         </span>
                        <span>
                            <a href="foo">888 798 0202</a>
                        </span>
                    </div>
                    <div className="right-site-list">
                        <Link to={'/login'}>My Account</Link>
                        <Link to={'/cart'}>Cart</Link>
                    </div>

                </header>

                <div className="container">
                    <div className="container__nav" id="nav">
                        <img src="https://store.hermanmiller.com/on/demandware.static/Sites-herman-miller-Site/-/default/dw92ba33db/images/logo.svg"
                             alt=""/>
                        <a href="foo">New</a>
                        <a href="foo">Office</a>
                        <a href="foo">Living</a>
                        <a href="foo">Dinning</a>
                        <a href="foo">Bedroom</a>
                        <a href="foo">Outdoor</a>
                        <a href="foo">Lighting</a>
                        <a href="foo">Accessories</a>
                        <div className="container__header-search-icon">

                            <input className="input-text" type="text" name="q" placeholder="Search"/>
                        </div>
                    </div>
                </div>

                <div className="loginForm">
                    <form onSubmit={handleSubmit(this.onSubmit2)}>
                        <div className="login-box">
                            <h1>Login</h1>
                            <div className="textbox">
                                {/*<i className="fas fa-user"></i>*/}
                                {/*<label>Username*</label>*/}
                                <Field type="text" placeholder="Username" name="userName" component={this.renderInput} />
                            </div>
                            <div className="textbox">
                                {/*<i className="fas fa-lock"></i>*/}
                                {/*<label>Password*</label>*/}
                                <Field type="text" placeholder="Password" name="password" component={this.renderInput} />
                            </div>
                            <input type="submit" className="btn" value="Sign in" />
                        </div>

                    </form>

                </div>
            </div>

        );
    }

}

const loginWrapped = reduxForm({
    form:'loginForm',
})(Login);

export default connect(null, {actFetchUser})(loginWrapped)