import React, {Component} from "react";
// import {FaTruck} from "react-icons/fa/index";
import {connect} from "react-redux"
import {Link} from "react-router-dom";
import {actAddToCart} from "../redux/actions/mainAction";
import login from "./login";


class BottomBar extends Component {

    state = {
        quantity: 1,
    };

    componentDidMount() {
        // console.log('bottom bar get product info ==>', this.props.unitePrice)
    }

    handleClick = () => {

        this.props.actAddToCart(this.props.unitProduct, this.state.quantity);
        // this.props.actAddToCart(this.state.quantity);
        // console.log('unit product ==>', this.props.unitProduct)
    };

    handleChange =  (event) => {
    	this.setState({
            quantity: event.target.value
        });
    };


    render() {
        if (this.props.unitProduct) {
            // let unitName = this.props.unitProduct.name
            let unitPrice = this.props.unitProduct.price;
            let fixedPrice = parseFloat(unitPrice).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

            // console.log('total price ===>', unitPrice)
            return (
                <div>
                    <div className="total-bar" id="sticky-bar">
                        <div className="totalbar-title">
                            <h3>{this.props.unitProduct.name}</h3>
                        </div>
                        <div className="totalbar-right">
                            <div className="total-bar-icon"><p><i></i></p></div>
                            <div className="total-bar-shipinfo"><p>Ready to ship in 2 weeks</p></div>
                            <div className="total-bar-amount"><p><span>C$ {fixedPrice}</span></p></div>
                            <input type="number" id="quantity-input" value={this.state.quantity}
                            	   onChange={this.handleChange}
                            />
                            {/*{console.log('on change ==>', this.state.quantity)}*/}

                            <button id="add-to-cart" onClick={this.handleClick}>Add To Cart</button>
                            {/*{console.log('add cart finish ==>', this.props.cartItem)}*/}


                        </div>
                    </div>
                </div>
            )
        }
    }
}

const mapStateToProps = state => {
    return {
        // productDetail: state.productReducer.productDetail,
        cartItem: state.cartReducer.cartItem
    }
};

// const mapDispatchToProps = {getProduct, addToCart};

export default connect(mapStateToProps,{actAddToCart})(BottomBar)

// export default BottomBar
