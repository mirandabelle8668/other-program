// 全局替换变量 command+r
import "../main.css"
import React from "react";
// import "./shoppingApps";
import "../pic/star-full.svg"
import {connect} from "react-redux";
import {actHomeProduct} from "../redux/actions/productAction";
import {Link} from "react-router-dom";


class Home extends React.Component{

    state = {
        homeProduct: [],
    };

    componentDidMount() {
        this.props.actHomeProduct();
    }



    render() {

            return (
                <div>
                    {/*<img src="star-full.svg" alt=""/>*/}
                    <div className="banner-sales">
                        <div className="banner-text">
                            Enjoy Free Shipping Sitewide + 0% Financing Available
                        </div>
                    </div>

                    <header>
                        <ul className="left-site-list">
                            <li className="active">
                                <Link to={'/'}>Store</Link>
                            </li>
                            <li>
                                <a href="foo">Contract</a>
                            </li>
                        </ul>
                        <div className="center-site-list">
                            <span>
                                <a href="foo">Customer Service</a>
                            </span>
                            <span>
                                <a href="foo">888 798 0202</a>
                            </span>
                        </div>
                        <div className="right-site-list">
                            <Link to={'/login'}>My Account</Link>
                            <Link to={'/cart'}>Cart</Link>
                        </div>
                    </header>

                    <div className="container">
                        <div className="container__nav" id="nav">
                            <img
                                src="https://store.hermanmiller.com/on/demandware.static/Sites-herman-miller-Site/-/default/dw92ba33db/images/logo.svg"
                                alt=""/>
                            <a href="foo">New</a>
                            <a href="foo">Office</a>
                            <a href="foo">Living</a>
                            <a href="foo">Dinning</a>
                            <a href="foo">Bedroom</a>
                            <a href="foo">Outdoor</a>
                            <a href="foo">Lighting</a>
                            <a href="foo">Accessories</a>
                            <div className="container__header-search-icon">
                                <input className="input-text" type="text" name="q" placeholder="Search"/>
                            </div>
                        </div>

                        <div className="container__content">
                            <div className="container__content__breadcrumb">
                                Home > Office > Office Chairs
                            </div>
                            <div className="container__content__title">
                                Office Chairs
                            </div>
                            <div className="container__content__sort-by">
                                <div className="container__content__sort-by__left-part">
                                    <label>Price</label>
                                    <label>Material</label>
                                    <label id="grid-sort-header-label" htmlFor="grid-sort-header">Sort By:</label>
                                    <div className="select-style">
                                        <select id="grid-sort-header">
                                            {/*select后面跟 value={this.state.xxx}*/}
                                            {/*option后面跟 value="Angular"*/}
                                            {/*option里面value有什么值select就会get什么值*/}
                                            <option>
                                                Featured Products
                                            </option>
                                            <option>
                                                Price: High to Low
                                            </option>
                                            <option>
                                                Price: Low to High
                                            </option>
                                            <option>
                                                Name: A to Z
                                            </option>
                                            <option>
                                                Name: Z to A
                                            </option>
                                            <option>
                                                Average Rating
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div className="show-number">Showing 7 of 7 items</div>
                            </div>


                            <div className="container__content__list">
                                <product-item>
                                    <div className="product-item">
                                        <div>
                                            <product-display>
                                                <div className="product-display">
                                                    <Link to={'/product'} className="product-display__base">
                                                        <img width="312"
                                                             src="https://s7d2.scene7.com/is/image/HermanMillerStore/b2c_3x4crop?$image_src=HermanMillerStore/AeronChair_defaultback2x&$b2c_1200x1600_jpeg$"
                                                             alt=""/>
                                                    </Link>
                                                </div>
                                            </product-display>

                                            <div className="product-review">
                                                <div className="rating">
                                                    <img className="rating-star" src="star-full.svg" alt=""/>
                                                    <img className="rating-star" src="star-full.svg" alt=""/>
                                                    <img className="rating-star" src="star-full.svg" alt=""/>
                                                    <img className="rating-star" src="star-full.svg" alt=""/>
                                                    <img className="rating-star" src="star-half.svg" alt=""/>
                                                </div>
                                                <h3>
                                                    Aeron Chair
                                                </h3>
                                                <div className="product-sales-price">C$ 1,245.00 - C$ 2,270.00</div>
                                                <div className="color-choosing">
                                                    <a href="foo">
                                                        <img
                                                            src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_studio_white_mineral"
                                                            alt=""/>
                                                    </a>
                                                    <a href="foo">
                                                        <img
                                                            src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_canyon"
                                                            alt=""/>
                                                    </a>
                                                    <a href="foo">
                                                        <img
                                                            src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_carbon"
                                                            alt=""/>
                                                    </a>
                                                    <a href="foo">
                                                        <img
                                                            src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_glacier"
                                                            alt=""/>
                                                    </a>
                                                    <a href="foo">
                                                        <img
                                                            src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_graphite"
                                                            alt=""/>
                                                    </a>
                                                    <a href="foo">
                                                        <img
                                                            src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_nightfall"
                                                            alt=""/>
                                                    </a>
                                                </div>
                                                <p>Free Shipping On Everything </p>
                                            </div>
                                        </div>
                                    </div>
                                </product-item>

                                {this.props.homeProduct.map((value, index) =>
                                    <product-item>
                                        <div className="product-item">
                                            <div>
                                                <product-display>
                                                    <div className="product-display" key={index}>
                                                        <Link to={`product/${value.fields.pk}`}
                                                              className="product-display__base">
                                                            {console.log('media -->', value.fields.fields.media.split('|'))}
                                                                    <img src={value.fields.fields.media.split('|')[0]}
                                                                         alt="#"
                                                                        style={{width: '312px', height:'416px'}}
                                                                    />
                                                        </Link>
                                                    </div>
                                                </product-display>

                                                <div className="product-review">
                                                    <div className="rating">
                                                        <img className="rating-star" src="star-full.svg" alt=""/>
                                                        <img className="rating-star" src="star-full.svg" alt=""/>
                                                        <img className="rating-star" src="star-full.svg" alt=""/>
                                                        <img className="rating-star" src="star-full.svg" alt=""/>
                                                        <img className="rating-star" src="star-half.svg" alt=""/>
                                                    </div>
                                                    <h3>
                                                        {value.fields.fields.name}
                                                    </h3>
                                                    <div
                                                        className="product-sales-price">C$ {value.fields.fields.price}</div>
                                                    <div className="color-choosing">
                                                        <a href="foo">
                                                            <img
                                                                src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_studio_white_mineral"
                                                                alt=""/>
                                                        </a>
                                                        <a href="foo">
                                                            <img
                                                                src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_canyon"
                                                                alt=""/>
                                                        </a>
                                                        <a href="foo">
                                                            <img
                                                                src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_carbon"
                                                                alt=""/>
                                                        </a>
                                                        <a href="foo">
                                                            <img
                                                                src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_glacier"
                                                                alt=""/>
                                                        </a>
                                                        <a href="foo">
                                                            <img
                                                                src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_graphite"
                                                                alt=""/>
                                                        </a>
                                                        <a href="foo">
                                                            <img
                                                                src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_frame_nightfall"
                                                                alt=""/>
                                                        </a>
                                                    </div>
                                                    <p>Free Shipping On Everything </p>
                                                </div>

                                            </div>
                                        </div>
                                    </product-item>
                                )}

                            </div>
                        </div>
                    </div>


                    <footer>
                        <div className="footer_section">
                            Customer Service
                            <a href="foo">Contact us</a>
                            <a href="foo">FAQ</a>
                            <a href="foo">Returns and Exchanges</a>
                            <a href="foo">Shipping and Delivery </a>
                            <a href="foo">Warranty and Service</a>
                            <a href="foo">Site Feedback</a>
                            <a href="foo">Track Your Order</a>
                        </div>
                        <div className="footer_section">
                            News & Resources
                            <a href="foo">For Business</a> <br/>
                            Locations
                            <a href="foo">Find a Retailer</a>
                            <a href="foo">Our New York Store</a>
                        </div>
                        <div className="footer_section">
                            About HmM
                            <a href="foo">About Us</a>
                            <a href="foo">HmM.com</a>
                            <a href="foo">Our Designers</a>
                            <a href="foo">Request A Catalog</a>
                            <a href="foo">Careers</a>
                            <a href="foo">Site Feedback</a>
                        </div>
                        <div className="footer_section">
                            <div className="footer_section__parent">
                                Join our mailing list
                            </div>
                            <div className="footer_section__parent">
                                Follow Us
                            </div>

                        </div>

                    </footer>

                </div>
            )
        }

}

const mapStateToProps = state => {

    // let homeProduct = state.productReducer.homeProduct;
    // console.log(homeProduct);
    return{
        homeProduct: state.productReducer.homeProduct,
        productDetail: state.productReducer.productDetail,
    }
};

export default connect(mapStateToProps, {actHomeProduct})(Home)