// shift + command + R 实现replaceAll
import React, {Component} from "react";
import '../main.css';
import {Field, reduxForm, FieldArray} from "redux-form";
import Multiselect from 'react-widgets/lib/Multiselect'
import 'react-widgets/dist/css/react-widgets.css'
import {actCreateProduct, actGetCategories} from "../redux/actions/mainAction";
import {actProductList} from "../redux/actions/productAction";
import {connect} from "react-redux";


class ProductAdd extends Component {

    state = {
        categories: [],
    };

    componentDidMount() {
        this.props.actGetCategories();
        this.props.actProductList()
    }

    // componentDidUpdate(prevProps, prevState, snapshot) {
    //     this.props.actGetCategories();
    // }


    getCategoryNum = (data) => {
        let categoryNum = [];
        for (let i = 0; i < data.length; i++){
            categoryNum.push(parseInt(data[i].split('.')[0]))
        }
        return categoryNum
    };

    onSubmit3 = async formValue2 => {
        let newProduct = {
            // "categories": this.getCategoryNum(formValue2.categories),
            "categories": this.getCategoryNum(formValue2.categories),
            "name": formValue2.name,
            "slug": formValue2.slug,
            "description": formValue2.description,
            "price": parseInt(formValue2.price),
            "isActive": true,
            "profileitem": formValue2.profileitem,
            "medias": formValue2.medias,
            "profiles": [],
            "isDeleted": false,

        };
        console.log("newProduct ---->", newProduct);
        console.log("formValue2 ---->", formValue2);
        await this.props.actCreateProduct(newProduct);
        // await this.props.actProductList()
        // 在action里面写了actProductList以后还要在主页面这里调用一下,只要写了action就必须在要渲染界面的代码页或者运行的代码页进行一下调用
        // 比如action里面所有的actXXX都在各自页面有call

    };

    renderMultiselect = ({input, data, rest, valueField, textField}) =>
        <Multiselect {...input}
                     onBlur={() => input.onBlur()}
                     value={input.value || []} // requires value to be an array
            // value={input.value || data.map(a => a.name)} // requires value to be an array
                     data={data}
            // valueField={valueField}
            // textField={textField},
                     {...rest}/>;
// />;

    renderProfile = ({fields, meta: {touched, error, submitFailed}}) => (
        <ul>
            <li>
                <button className="btn btn-outline-primary" type="button" onClick={() => fields.push({})}>Add Profile</button>
                {(touched || submitFailed) && error && <span>{error}</span>}
            </li>
            {fields.map((profile, index) =>
                <li key={index}>
                    <button
                        className="btn btn-outline-primary"
                        type="button"
                        title="Remove Profile"
                        onClick={() => fields.remove(index)}>
                        Remove Profile
                    </button>
                    <Field
                        label={`Profile #${index + 1}`}
                        name={`${profile}.profile`}
                        type="text"
                        component={this.renderField}/>
                    <FieldArray name={`${profile}.item`} component={this.renderItem}/>
                </li>
            )}
        </ul>
    );

    renderItem = ({fields, meta: {error}}) => (
        <ul>
            <li>
                <button className="btn btn-outline-primary" type="button" onClick={() => fields.push()}>Add Item</button>
            </li>
            {fields.map((item, index) =>
                <li key={index}>
                    <button
                        className="btn btn-outline-primary"
                        type="button"
                        title="Remove item"
                        onClick={() => fields.remove(index)}>
                        Remove item
                    </button>
                    <Field
                        name={`${item}.name`}
                        type="text"
                        component={this.renderField}
                        label="name"/>
                    <Field
                        name={`${item}.price`}
                        type="text"
                        component={this.renderField}
                        label="price"/>
                </li>
            )}
            {error && <li className="error">{error}</li>}
        </ul>
    );

    renderField = ({input, label, type, meta: {touched, error}}) => (
        <div>
            <label>{label}</label>
            <div>
                <input className="form-control" {...input} type={type} placeholder={label}/>
                {touched && error && <span>{error}</span>}
            </div>
        </div>
    );

    renderMedia = ({fields, meta: {error}}) => (
        <ul>
            <li>
                <button className="btn btn-outline-primary" type="button" onClick={() => fields.push()}>Add Picture</button>
            </li>
            {fields.map((media, index) =>
                <li key={index}>
                    <button
                        className="btn btn-outline-primary"
                        type="button"
                        title="Remove Picture"
                        onClick={() => fields.remove(index)}>
                        Remove Picture
                    </button>
                    <Field
                        name={media}
                        type="text"
                        component={this.renderField}
                        label={`picture #${index + 1}`}/>
                </li>
            )}
            {error && <li className="error">{error}</li>}
        </ul>
    );


    render() {
        const {handleSubmit, pristine, reset, submitting} = this.props;
        return (
            <div className="productAdd">
                <div>
                    {/*mt-3表格上部分留白多少*/}
                    <div className="form-header">
                        <h5>
                            Product Input
                        </h5>
                    </div>

                    <div className="formDivBody">
                        <form onSubmit={handleSubmit(this.onSubmit3)}>
                            <div className="form-input">
                                <label>Categories</label>
                                <Field
                                    name="categories"
                                    component={this.renderMultiselect}
                                    data={this.props.categories.map((value, index) => {
                                        return [`${value.pk}.${value.fields.friendlyName}`]
                                    })}/>
                            </div>
                            <div className="form-group-all">
                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <Field label="Name" type="text" className="form-control" id="inputEmail4"
                                               name="name"
                                               component={this.renderField}/>
                                    </div>
                                    <div className="form-group col-md-6">
                                        <Field label="Slug" type="text" className="form-control" id="inputPassword4"
                                               name="slug"
                                               component={this.renderField}/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <Field label="Description" type="text" className="form-control" id="inputAddress"
                                           name="description" placeholder="Some description..."
                                           component={this.renderField}/>
                                </div>
                                <div className="form-row">
                                    <div className="form-group col-md-6">
                                        <Field label="Price" type="text" className="form-control" id="inputAddress2"
                                               name="price" component={this.renderField}/>
                                    </div>
                                    <div className="form-group col-md-6">
                                        <Field label="Activated" type="text" className="form-control" name="isActive"
                                               component={this.renderField}/>
                                    </div>
                                </div>

                            </div>
                            <FieldArray name="profileitem" component={this.renderProfile}/>
                            <FieldArray name="medias" component={this.renderMedia}/>
                            <div>
                                <button className="btn btn-outline-primary" type="submit"
                                        disabled={pristine || submitting}>Submit
                                </button>
                                <button className="btn btn-outline-primary" type="button"
                                        disabled={pristine || submitting} onClick={reset}>Reset Values
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div className="productShowUp" style={{margin:'50px 100px'}}>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Slug</th>
                            <th scope="col">Price</th>
                            <th scope="col">PK</th>
                            <th scope="col">Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.productList.map((value, index) =>
                            <tr>
                                {/*<th scope="col">1</th>*/}
                                <td data-label="Number">{index + 1}</td>
                                <td data-label="Name">{value.fields.fields.name}</td>
                                <td data-label="Slug">{value.fields.fields.slug}</td>
                                <td data-label="Price">{value.fields.fields.price}</td>
                                <td data-label="PK">{value.fields.pk}</td>
                                <td><i className="far fa-trash-alt btn btn-danger"></i></td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

const productWrapped = reduxForm({
    form: 'productAddForm'
})(ProductAdd);

const mapStateToProps = state => {

    // let productList= state.productReducer.productList;
    // console.log('product add page - product list ===>', productList);
    return{
        categories: state.categoryReducer.categories,
        productList:state.productReducer.productList

    }
};

export default connect(mapStateToProps, {actCreateProduct, actGetCategories, actProductList})(productWrapped)
