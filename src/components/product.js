import "../main.css"
import React from "react";
import {connect} from "react-redux";
import {actFetchOneProduct} from "../redux/actions/productAction";
import login from "./login";
import "./bottomBar"
import BottomBar from "./bottomBar";
import Cart from "./cart";
import {Link} from "react-router-dom";

class Product extends React.Component{

    // productDetail = null;
    // componentDidMount() {
    //     let id = this.props.match.params.id;
    //     this.props.actProductDetail(id)
    //     console.log('productDetail', this.props.productDetail.fields?.fields)
    //
    // }

    componentDidMount() {
        let id = this.props.match.params.id;
        this.props.actFetchOneProduct(id);
        // console.log("product detail page ==>", id)
    }


    render() {

        //     let medialinks = this.props.productDetail?.data?.fields?.fields?.media
        // console.log('media links--->>>', medialinks)
        //     // let medialink = medialinks.split('|')
        //     console.log('!!!', this.props.data?.productDetail.fields.fields.slug)
        // console.log('productDetail', this.props.productDetail)

        if (this.props.productDetail) {

            let medialink = this.props.productDetail.medias;
            medialink = medialink ?  medialink : '';
            console.log('productDetail whole info===>', this.props.productDetail);
            let item = this.props.productDetail.profileitem;
            item = item ? item : '';
            console.log('productDetail.profileitem===>', this.props.productDetail.profileitem);
            let price = this.props.productDetail.price;
            let twoDigPrice = parseFloat(price).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");


            return (
                <div>
                    <div className="banner-sales">
                        <div className="banner-text">
                            Enjoy Free Shipping Sitewide + 0% Financing Available
                        </div>
                    </div>

                    <header>
                        <ul className="left-site-list">
                            <li className="active">
                                <Link to={'/'}>Store</Link>
                            </li>
                            <li>
                                <a href="foo">Contract</a>
                            </li>
                        </ul>
                        <div className="center-site-list">
                            <span>
                                 <a href="foo">Customer Service</a>
                            </span>
                            <span>
                                 <a href="foo">888 798 0202</a>
                            </span>
                        </div>
                        <div className="right-site-list">
                            <Link to={'/login'}>My Account</Link>
                            <Link to={'/cart'}>Cart</Link>
                        </div>
                    </header>

                    <div className="container">
                        <div className="container__nav" id="nav">
                            <img
                                src="https://store.hermanmiller.com/on/demandware.static/Sites-herman-miller-Site/-/default/dw92ba33db/images/logo.svg"
                                alt=""/>
                            <a href="foo">New</a>
                            <a href="foo">Office</a>
                            <a href="foo">Living</a>
                            <a href="foo">Dinning</a>
                            <a href="foo">Bedroom</a>
                            <a href="foo">Outdoor</a>
                            <a href="foo">Lighting</a>
                            <a href="foo">Accessories</a>
                            <div className="container__header-search-icon">
                                <input className="input-text" type="text" name="q" placeholder="Search"/>
                            </div>
                        </div>

                        <div className="container__content">
                            <div className="container__content__breadcrumb">
                                Home > Office > Office Chairs
                            </div>

                            <div>
                                <div className="container__content__image" id="content-image">
                                    <div className="product-gallery">
                                        {this.props.productDetail?.medias?.map((media, index) =>
                                            <a href="foo">
                                                <img
                                                    src={media}
                                                    // src={this.props.productDetail.medias[0]}
                                                    alt=""/>
                                            </a>
                                        )}
                                    </div>
                                    <img
                                        src={medialink[0]}
                                        alt=""/>
                                </div>


                                <div className="product-detail">
                                    <h1>
                                        {this.props.productDetail.name}
                                    </h1>
                                    <p className="product-designer">
                                        {this.props.productDetail.slug}
                                    </p>
                                    <div className="rating">
                                        <img className="rating-star" src="star-full.svg" alt=""/>
                                        <img className="rating-star" src="star-full.svg" alt=""/>
                                        <img className="rating-star" src="star-full.svg" alt=""/>
                                        <img className="rating-star" src="star-full.svg" alt=""/>
                                        <img className="rating-star" src="star-half.svg" alt=""/>
                                    </div>
                                    <div className="product-price">
                                        {/*C$ 1,745.00*/}
                                        C$ {twoDigPrice}
                                    </div>
                                    <div className="warranty-info">
                                        <a href="foo">
                                            <i className="fas fa-check" style={{color: '#e62b00'}}>
                                            </i>
                                            <span>12-Year Warranty</span>
                                        </a>
                                        <a href="foo">
                                            <i className="fas fa-check" style={{color: '#e62b00'}}>
                                            </i>
                                            <span>Free Standard Shipping</span>
                                        </a>
                                        <a href="foo">
                                            <i className="fas fa-check" style={{color: '#e62b00'}}>
                                            </i>
                                            <span>130-Day No Hassle Return</span>
                                        </a>
                                    </div>
                                    <a className="promo" href="foo">
                                        Free Shipping On Everything
                                    </a>
                                    <label>
                                        Frame / Base
                                    </label>
                                    <div className="selected-swatch">
                                        Graphite / Graphite
                                    </div>
                                    <div className="frame-base">
                                        <a href="foo">
                                            <img
                                                src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_framebase_graphite_polished_aluminum"
                                                alt=""/>
                                        </a>
                                        <a href="foo">
                                            <img
                                                src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_framebase_mineral_satin_aluminum"
                                                alt=""/>
                                        </a>
                                        <a href="foo">
                                            <img
                                                src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_framebase_mineral_polished_aluminum"
                                                alt=""/>
                                        </a>
                                        <a href="foo">
                                            <img
                                                src="https://s7d2.scene7.com/is/image/HermanMillerStore/s_framebase_graphite_graphite"
                                                alt=""/>
                                        </a>
                                    </div>
                                    <button>
                                        Request Free Swatches
                                    </button>
                                    <div>
                                        {this.props.productDetail?.profileitem?.map((profile, index) =>
                                            <div>
                                                <label>
                                                    {profile.profile}
                                                    {/*Size*/}
                                                </label>
                                                {profile.item.map((item, index) =>
                                                    <div>
                                                        <form className="value-choosing">
                                                            <input type="radio" name="size" value="Size A - Small"/>
                                                            {/*Size A - Small*/}
                                                            {item.name}
                                                            <br>
                                                        </br>
                                                        {/*    <input type="radio" name="size" value="Size B - Medium"/>*/}
                                                        {/*    Size B - Medium*/}
                                                        {/*    <br>*/}
                                                        {/*</br>*/}
                                                        {/*    <input type="radio" name="size" value="fSize C - Large"/>*/}
                                                        {/*    Size C - Large*/}
                                                        </form>


                                                        {/*<label>*/}
                                                        {/*    /!*Back Support*!/*/}
                                                        {/*    {profile.profile}*/}
                                                        {/*</label>*/}
                                                        {/*<form className="value-choosing">*/}
                                                        {/*    <input type="radio" name="size" value="Basic Back Support"/>*/}
                                                        {/*    /!*Basic Back Support*!/*/}
                                                        {/*    {item.name}*/}
                                                        {/*    <br>*/}
                                                        {/*</br>*/}
                                                        {/*/!*    <input type="radio" name="size" value="Adjustable Lumbar Support"/>*!/*/}
                                                        {/*/!*    Adjustable Lumbar Support*!/*/}
                                                        {/*/!*    <br>*!/*/}
                                                        {/*/!*</br>*!/*/}
                                                        {/*/!*    <input type="radio" name="size" value="Adjustable Posturefit SL"/>*!/*/}
                                                        {/*/!*    Adjustable Posturefit SL*!/*/}
                                                        {/*</form>*/}
                                                    </div>
                                                )}

                                            </div>
                                        )}

                                    </div>
                                </div>



                            </div>
                            {/*)}*/}


                            <div className="description">
                                <h4>Description</h4>
                                <div className="description-list">
                                    <div className="left-side">
                                        {/*The Aeron Chair combines a deep knowledge of human-centered design with*/}
                                        {/*cutting-edge*/}
                                        {/*technology. With over 7 million sold, our most admired and recognized work chair*/}
                                        {/*still sets the benchmark for ergonomic comfort more than 20 years after its*/}
                                        {/*debut.*/}
                                        {this.props.productDetail.description}
                                    </div>
                                    <div className="right-side">
                                        <h6>
                                            Key Features
                                        </h6>
                                        <ul className="right-side-description-list">
                                            <li>12-year warranty</li>
                                            <li>Inclusive design with 3 distinct sizes</li>
                                            <li>Breathable seat and back</li>
                                            <li>Pellicle 8Z® provides 8 zones of varying tension for sophisticated
                                                support
                                            </li>
                                            <li>Made in Michigan at a 100% green-energy facility</li>
                                            <li>Adjustable tilt and seat angle</li>
                                            <li>Superior back support</li>
                                            <li>Fully adjustable arms (height, depth and angle)</li>
                                            <li>For questions about lead times, in-stock options or delivery please give
                                                us
                                                a call at 888.798.0202
                                            </li>
                                        </ul>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <BottomBar unitProduct={this.props.productDetail}></BottomBar>

                    <footer>
                        <div className="footer_section">
                            Customer Service
                            <a href="foo">Contact us</a>
                            <a href="foo">FAQ</a>
                            <a href="foo">Returns and Exchanges</a>
                            <a href="foo">Shipping and Delivery </a>
                            <a href="foo">Warranty and Service</a>
                            <a href="foo">Site Feedback</a>
                            <a href="foo">Track Your Order</a>
                        </div>
                        <div className="footer_section">
                            News & Resources
                            <a href="foo">For Business</a> <br/>
                            Locations
                            <a href="foo">Find a Retailer</a>
                            <a href="foo">Our New York Store</a>
                        </div>
                        <div className="footer_section">
                            About HmM
                            <a href="foo">About Us</a>
                            <a href="foo">HmM.com</a>
                            <a href="foo">Our Designers</a>
                            <a href="foo">Request A Catalog</a>
                            <a href="foo">Careers</a>
                            <a href="foo">Site Feedback</a>
                        </div>
                        <div className="footer_section">
                            <div className="footer_section__parent">
                                Join our mailing list
                            </div>
                            <div className="footer_section__parent">
                                Follow Us
                            </div>
                        </div>

                    </footer>
                </div>
            )
        }
    }
}


const mapStateToProps = (state) => {

    let singleProduct = state.productReducer.productDetail;
    let bigImage = '';
    if (singleProduct.medias && Array.isArray(singleProduct.medias) && singleProduct.medias.length > 0) {
        bigImage = singleProduct.medias[0]
    }
    return {
        productDetail: state.productReducer.productDetail,
        singleProduct,
        bigImage,

    };
};

export default connect(mapStateToProps, {actFetchOneProduct})(Product)

