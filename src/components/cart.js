import React from "react";
import "../main.css"
import {connect} from "react-redux";
import {actAddToCart} from "../redux/actions/mainAction";
import {Link} from "react-router-dom";
import {Field} from "redux-form";

class Cart extends React.Component{

    state = {
        // cartItem: []
        quantity: '',
    };


    componentDidMount() {
        if(this.props.cartItem) {
            // this.props.cartItem.map((value => {
            //
            // }))
            console.log('cart page mounted ==>', this.props.cartItem);
            console.log('length: --->');
            // this.setState({quantity: undefined})
        }
    }

    handleChange =  (event) => {
        this.setState({
            quantity: event.target.value
        });
    };

    render() {
        // if (this.props.cartItem.length) {
        //     console.log('cart page mounted 22 ==>', this.props.cartItem)
        // let price = this.props.productDetail.price;
        // let twoDigPrice = parseFloat(price).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");



        return (
                <div>
                    <div className="banner-sales">
                        <div className="banner-text">
                            Enjoy Free Shipping Sitewide + 0% Financing Available
                        </div>
                    </div>

                    <header>
                        <ul className="left-site-list">
                            <li className="active">
                                <Link to={'/'}>Store</Link>
                            </li>
                            <li>
                                <a href="foo">Contract</a>
                            </li>
                        </ul>
                        <div className="center-site-list">
                            <span>
                                <a href="foo">Customer Service</a>
                            </span>
                            <span>
                                <a href="foo">888 798 0202</a>
                            </span>
                        </div>
                        <div className="right-site-list">
                            <Link to={'/login'}>My Account</Link>
                            <Link to={'/cart'}>Cart</Link>
                        </div>
                    </header>

                    <div className="container">
                        <div className="container__nav" id="nav">
                            <img
                                src="https://store.hermanmiller.com/on/demandware.static/Sites-herman-miller-Site/-/default/dw92ba33db/images/logo.svg"
                                alt=""/>
                            <a href="foo">New</a>
                            <a href="foo">Office</a>
                            <a href="foo">Living</a>
                            <a href="foo">Dinning</a>
                            <a href="foo">Bedroom</a>
                            <a href="foo">Outdoor</a>
                            <a href="foo">Lighting</a>
                            <a href="foo">Accessories</a>
                            <div className="container__header-search-icon">
                                <input className="input-text" type="text" name="q" placeholder="Search"/>
                            </div>
                        </div>

                        <div className="container__content">
                            <div className="container__content__breadcrumb">
                                Home > Office > Office Chairs
                            </div>

                            <div className="cart">
                                <h1>My Cart</h1>
                                <fieldset>
                                    <table className="table">
                                        <thead>
                                        <tr>
                                            <th scope="col" colSpan="2">Product Information</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Remove</th>
                                            <th scope="col">Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                         {this.props.cartItem?.map((item, index) =>
                                            <tr className="cart-row" key={item.id}>
                                                <td className="item-image">
                                                    {console.log('item.priceeeeeeeee', item)}
                                                    {/*{console.log('item.media ##########', item.medias[0])}*/}
                                                    {/*{oneProduct.medias?.map((media) =>*/}
                                                        <img
                                                            // src="https://s7d2.scene7.com/is/image/HermanMillerStore/b2c_3x4crop?$image_src=HermanMillerStore/AeronChair_AER1C23DWALPG1G1G1BBBK23103_Front&$b2c_360x480_jpeg$"
                                                            src={item.medias[0]}
                                                            alt=""
                                                        />
                                                     {/*)}*/}
                                                </td>
                                                <td className="item-details">
                                                    <p>
                                                        AER1C23DWALPG1G1G1BBBK23103
                                                        Frame / Base: Graphite / Graphite
                                                        Size: Size C - Large
                                                        Back Support: Adjustable Posturefit SL
                                                        Tilt: Tilt Limiter and Seat Angle
                                                        Arms: Fully Adjustable Arms
                                                        Armpad: Standard
                                                        Caster: Carpet Caster
                                                    </p>
                                                </td>
                                                <td className="item-price">
                                                    {/*C$ 1,745.00*/}
                                                    C$ {parseFloat(item.price).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}
                                                </td>
                                                <td className="item-quantity">
                                                    {console.log('item.quantity &&&&&&&', item.quantity)}
                                                    {/*{this.setState({quantity: {id: item.id, val: item.quantity}})}*/}
                                                    <input defaultValue={item.quantity}  onChange={this.handleChange}
                                                    />
                                                    {console.log('quantity state^^^^^^', this.state.quantity)}
                                                </td>
                                                <td className="item-remove">remove</td>
                                                <td className="item-total">C$ 1,745.00</td>
                                            </tr>
                                        )}
                                        </tbody>
                                    </table>
                                </fieldset>

                                <div className="cart-footer">
                                    <div className="guarantee">
                                        <h4>100% Satisfaction Guarantee</h4>
                                        <p>
                                            If you are not 100% satisfied with your purchase from the
                                            Herman Miller Store, you can return your order to us for an
                                            exchange or a full refund within 30 days of delivery.
                                            Yes, a full refund, meaning we'll give you all your money back
                                            for the product, shipping, and tax. If for whatever reason
                                            you're not happy with your purchase, contact our Customer
                                            Engagement team to arrange for your return or exchange.
                                        </p>
                                    </div>
                                    <div className="checkout">
                                        <table className="order-total-table">
                                            <tbody>
                                            <tr className="order-subtotal">
                                                <td>Subtotal:</td>
                                                <td>C$ 2,525.00</td>
                                            </tr>
                                            <tr className="order-sales-tax">
                                                <td>Estimated Tax:</td>
                                                <td>C$ -.-</td>
                                            </tr>
                                            <tr className="order-shopping">
                                                <td>Delivery: FedEx</td>
                                                <td>C$ -.-</td>
                                            </tr>
                                            <tr className="order-total">
                                                <td>Estimated Total:</td>
                                                <td>C$ 2,525.00</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <button className="checkout">checkout</button>

                                    </div>

                                </div>

                            </div>
                        </div>


                    </div>

                    <footer>
                        <div className="footer_section">
                            Customer Service
                            <a href="foo">Contact us</a>
                            <a href="foo">FAQ</a>
                            <a href="foo">Returns and Exchanges</a>
                            <a href="foo">Shipping and Delivery </a>
                            <a href="foo">Warranty and Service</a>
                            <a href="foo">Site Feedback</a>
                            <a href="foo">Track Your Order</a>
                        </div>
                        <div className="footer_section">
                            News & Resources
                            <a href="foo">For Business</a> <br/>
                            Locations
                            <a href="foo">Find a Retailer</a>
                            <a href="foo">Our New York Store</a>
                        </div>
                        <div className="footer_section">
                            About HmM
                            <a href="foo">About Us</a>
                            <a href="foo">HmM.com</a>
                            <a href="foo">Our Designers</a>
                            <a href="foo">Request A Catalog</a>
                            <a href="foo">Careers</a>
                            <a href="foo">Site Feedback</a>
                        </div>
                        <div className="footer_section">
                            <div className="footer_section__parent">
                                Join our mailing list
                            </div>
                            <div className="footer_section__parent">
                                Follow Us
                            </div>
                        </div>

                    </footer>
                </div>
            )
         // }
    }

}

const mapStateToProps = state => {
    return {
        cartItem: state.cartReducer.cartItem,
        productDetail: state.productReducer.productDetail
    }
};

export default connect(mapStateToProps)(Cart)


// export default Cart